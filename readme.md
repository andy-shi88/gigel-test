### User profile simple rest

#### specs
- database: mysql
- language: python 3

#### prepare
- create virtualenv `virtualenv -p python3 env`
- activate the virtual environment `. env/bin/activate`
- install all dependencies needed and listed in `requirements.txt` by `pip install -r requirements.txt`
  - the following may come in handy if error encountered during dependencies instalation 
   - https://stackoverflow.com/a/51315202/7616247 (mac issue on mysqlclient) or https://github.com/PyMySQL/mysqlclient-python/issues/169#issuecomment-306821834 

- copy `.env.example` to `.env`
- run database schema migration. *[remember to update .env file to point to existing db (create one if does not exist) and also credential configuration]

#### run
- run development mode by `python main.py`


#### development
- ##### migration
  all schema migration (including its config) is in `./alembic` directory.
  - `alembic revision -m 'migration title'` to create migration file
  - `alembic upgrade head` to run migration to top
  - `alembic downgrade -1` rollback 1 migration [1 could be update to `n` for n step back]
