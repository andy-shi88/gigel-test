'''
api routes
'''
from flask import Blueprint, request, jsonify
from app.controllers import user_controller
from app.middlewares.auth import token_required


api = Blueprint('api', __name__)

@api.route('/userprofile', methods=['GET'])
def get_user_profiles(*args, **kwargs):
  return user_controller.browse(request)

@api.route('/register', methods=['POST'])
def register_user(*args, **kwargs):
  return user_controller.add(request)

@api.route('/userprofile/<user_id>', methods=['GET'])
def get_user_profile(user_id, *args, **kwargs):
  return user_controller.read(user_id, request)

@api.route('/userprofile', methods=['PATCH', 'UPDATE'])
@token_required
def update_user_profile(*args, **kwargs):
  user_id = kwargs['user']['id']
  return user_controller.edit(user_id, request)

@api.route('/userprofile/updatePassword', methods=['PATCH', 'UPDATE'])
@token_required
def update_user_password(*args, **kwargs):
  user_id = kwargs['user']['id']
  return user_controller.update_password(user_id, request)

@api.route('/login', methods=['POST'])
def login(*args, **kwargs):
  return user_controller.login(request)
