import os
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)


def verify_auth_token(token: str) -> dict:
  """verify jw token
  :param str token: jwt string
  :return dict: payload of token
  """
  s = Serializer(os.getenv('SECRET_KEY'))
  try:
      data = s.loads(token)
  except Exception as e:
      raise e
  return data

def generate_auth_token(user_data: dict, expiration: int=3600 * 24 * 7) -> str:
  """Generate auth token
  :param dict user_data: user data to inject to the token
  :param int expiration: default to 1 week
  :return str: jwt string
  """
  s = Serializer(os.getenv('SECRET_KEY'), expires_in=expiration)
  return s.dumps(user_data)