import bcrypt, secrets

def generate_password(password: str, salt: str) -> str:
  """Generate hashed salted password by hardcoded config
  :param str password: user password
  :param str salt: user salt
  :return str: hashed password
  """
  return bcrypt.hashpw(password+salt, bcrypt.gensalt(12))

def parse_password(input_pass: str, user_salt: str, db_hash: str) -> bool:
  """Parse user password
  :param str input_pass: user raw password
  :param str user_salt: user stored salt string
  :param str db_hash: stored user password hash
  :return bool: match(True) or not match(False)
  """

  return bcrypt.checkpw(input_pass+user_salt, db_hash)

def generate_salt() -> str:
  """Generate user salt."""
  return secrets.token_hex(32)