class Response(object):
  """Response object class."""

  def __init__(self):
    self.data = None
    self.links = {'page': None, 'total': None, 'rows': None}
    self.meta = {'success': True, 'message': 'api call succeeded'}

  def set_message(self, message: str):
    self.meta['message'] = message
  
  def set_success(self, success: bool):
    self.meta['success'] = success
  
  def set_links(self, links: dict):
    self.links = links
  
  def set_data(self, data: dict):
    self.data = data
