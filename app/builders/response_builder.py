from .response import Response

class ResponseBuilder():
  """Response builder class."""

  def __init__(self):
    self.response = Response()
  
  def build(self) -> Response:
    return self.response
  
  def set_message(self, message: str):
    self.response.set_message(message)
    return self
  
  def set_data(self, data: dict):
    self.response.set_data(data)
    return self
  
  def set_links(self, links: dict):
    self.response.set_links(links)
    return self

  def set_success(self, success: bool):
    self.response.set_success(success)
    return self
