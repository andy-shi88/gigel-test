import datetime
from app.utils.password_util import generate_password, parse_password, generate_salt
from app.utils.token_util import generate_auth_token
from app.models import db
from app.models.user import User

class UserService:
  """User resource service class
  - register(): user registration
  - update_password(): update user password
  - update_profile(): update user profile information
  """

  def __init__(self):
    """UserService constructor."""
  
  def get_users(self, page: int, limit: int):
    """get list of user profile
    :param int page: page number / offset
    :param int limit: number of row to return
    :return array[dict]: user profile information in range of page-limit
    """
    users = db.session.query(User).paginate(page, limit)
    result = list(map(lambda x: x.to_json(), users.items))
    return result

  def get_user(self, user_id: int):
    """get user by user id
    :param int user_id: id of user
    :return dict: user profile information
    """
    result = db.session.query(User).filter_by(id=user_id).first()
    if result is None:
      raise Exception('NotFound')
    return result.to_json()

  def register(self, payload: dict) -> dict:
    """register user account and information.
    :param dict payload: user information detail
    :return dict: created user information
    """
    user_exist = db.session.query(User).filter_by(email=payload['email']).first()
    if user_exist is not None:
      raise Exception('UserAlreadyExist')
    salt = generate_salt()
    hashed_password = generate_password(payload['password'], salt)
    user = User()
    user.password = hashed_password
    user.email = payload['email']
    user.name = payload['name']
    user.phone = payload['phone']
    user.salt = salt
    try:
      db.session.add(user)
      db.session.commit()
      return user.to_json()
    except Exception as e:
      raise e
  
  def update_password(self, user_id: int, old_password: str, new_password: str) -> dict:
    """update user password
    :param str old_password: user old password
    :param str new_password: user new password
    :return dict: user information
    """
    user = db.session.query(User).filter_by(id=user_id)
    user_data = user.first()
    if user_data is None:
      raise Exception('NotFound')
    old_pass_match = parse_password(old_password, user_data.salt, user_data.password)
    if old_pass_match is False:
      raise Exception('IncorrectOldPassword')
    salt = generate_salt()
    new_hash = generate_password(new_password, salt)
    try:
      user.update({
        'salt': salt,
        'password': new_hash,
        'updated_at': datetime.datetime.now()
      })
      db.session.commit()
      return user.first().to_json()
    except Exception as e:
      raise e

  def update_profile(self, user_id: int, payload: dict) -> dict:
    """update user information
    :param int user_id: user id to be updated
    :param dict payload: new user information
    :return dict: updated user information
    """
    user = db.session.query(
				User).filter_by(id=user_id)
    if user.first() is None:
      raise Exception('NotFound')
    try:
      user.update({
        'name': payload['name'],
        'email': payload['email'],
        'phone': payload['phone'],
        'updated_at': datetime.datetime.now()
      })
      db.session.commit()
      return user.first().to_json()
    except Exception as e:
      raise e

  def login(self, payload: dict) -> dict:
    """Generate user jw token
    :param dict payload: user payload [password, email]
    :return dict: token object
    """
    user = db.session.query(User).filter_by(email=payload['email'])
    user_data = user.first()
    if user_data is None:
      raise Exception('NotFound')
    pass_match = parse_password(payload['password'], user_data.salt, user_data.password)
    if pass_match is False:
      raise Exception('InvalidEmailOrPassword')
    # generate token here
    token = generate_auth_token({'id': user_data.id, 'email': user_data.email}).decode()
    # to simplify stuff, I omit refresh_token for now.
    return {'access_token': token}
