import datetime
# from werkzeug.security import generate_password_hash, check_password_hash
# import classes
from app.models import db

class User(db.Model):
    # table name
    __tablename__ = 'user_profile'

    # columns definitions
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    email = db.Column(db.String, index=True, unique=True)
    password = db.Column(db.String)
    phone = db.Column(db.String, unique=True)
    salt = db.Column(db.String)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)
    deleted_at = None   

    def __init__(self):
        self.created_at = datetime.datetime.now()
        self.updated_at = datetime.datetime.now()

    def to_json(self):
        return {
            'id': self.id,
            'nama': self.name,
            'email': self.email,    
            'phone': self.phone,
            'created_at': self.created_at,
            'updated_at': self.updated_at,
            'deleted_at': self.deleted_at,
        }