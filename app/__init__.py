from flask import Flask
from app.routes.api import api
from app.models import db

def create_app(configuration):
    app = Flask(__name__)
    # set configuration
    app.config.from_object(configuration)
    db.init_app(app)
    # register handlers to api/v1
    app.register_blueprint(api, url_prefix=app.config['API_BASE_URL'])
    return app
