from app.models import db
from functools import wraps
from flask import jsonify, request, current_app, Response, json
from itsdangerous import (TimedJSONWebSignatureSerializer 
	as Serializer, BadSignature, SignatureExpired
	)
from app.utils.token_util import verify_auth_token
from app.models.user import User

def token_required(f):
  @wraps(f)
  def decorated(*args, **kwargs):
    if 'Authorization' not in request.headers:
      return Response(json.dumps({'message': 'Authorization header is missing'}), status=401, mimetype='application/json')
    token = request.headers['Authorization']
    try:
      result = verify_auth_token(token[7:])
      kwargs['user'] = result
    except SignatureExpired:
      return Response(json.dumps({'message': 'Token is missing'}), status=401, mimetype='application/json')
    except BadSignature:
      return Response(json.dumps({'message': 'Token is invalid'}), status=401, mimetype='application/json')
    except Exception as e:
      return Response(json.dumps({'message': 'Token is invalid'}), status=401, mimetype='application/json')
    return f(*args, **kwargs)
  return decorated
