import logging
from app.controllers.base_controller import BaseController
from app.services.user_service import UserService
from app.builders.response_builder import ResponseBuilder

logger = logging.getLogger(__name__)

class UserController(BaseController):
	"""User resource controller."""

	def __init__(self):
		self.service = UserService()

	def browse(self, request):
		page = request.args.get('page') if request.args.get('page') else 1 
		limit = request.args.get('limit') if request.args.get('limit') else 10
		users = self.service.get_users(page, limit)
		response = ResponseBuilder() \
			.set_message('user fetched successfully') \
			.set_data(users) \
			.set_links({'page': page, 'rows': limit, 'total': 10}) \
			.build()
		return BaseController.json_response(response.__dict__, 200)

	def read(self, user_id: int, request):
		try:
			user = self.service.get_user(user_id)
			response = ResponseBuilder() \
				.set_message('user fetched successfully') \
				.set_data(user) \
				.build()
			return BaseController.json_response(response.__dict__, 200)
		except Exception as e:
			response_builder = ResponseBuilder().set_success(False)
			if e.args[0] == 'NotFound':
				response = response_builder \
					.set_message('user not found') \
					.build()
				return BaseController.json_response(response.__dict__, 404)
			response = response_builder.set_message('unknown error occurred, please try again later').build()
			return BaseController.json_response(response.__dict__, 500)
	
	def edit(self, user_id: int, request):
		email = request.form['email'] if 'email' in request.form else None
		name = request.form['name'] if 'name' in request.form else None
		phone = request.form['phone'] if 'phone' in request.form else None
		if None in [email, name, phone]:
			response = ResponseBuilder() \
				.set_message('invalid payload provided') \
				.set_data(None) \
				.set_success(False) \
				.build()
			return BaseController.json_response(response.__dict__, 422)
		try:
			user = self.service.update_profile(user_id, { 'email': email, 'name': name, 'phone': phone })
			response = ResponseBuilder() \
				.set_message('user updated successfully') \
				.set_data(user) \
				.build()
			return BaseController.json_response(response.__dict__, 200)
		except Exception as e:
			response_builder = ResponseBuilder() \
				.set_success(False)
			if e.args[0] == 'NotFound':
				response = response_builder.set_message('user does not exist').build()
				return BaseController.json_response(response.__dict__, 404)			
			response = response_builder.set_message('unknown error occured, please try again later').build()
			return BaseController.json_response(response.__dict__, 500)
	

	def update_password(self, user_id: int, request):
		old_password = request.form['old_password'] if 'old_password' in request.form else None
		new_password = request.form['new_password'] if 'new_password' in request.form else None
		response_builder = ResponseBuilder()
		if None in [old_password, new_password]:
			response = response_builder.set_success(False).set_message('invalid payload').build()
			return BaseController.json_response(response.__dict__, 422)
		try:
			user = self.service.update_password(user_id, old_password, new_password)
			response = response_builder.set_data(user).set_message('password updated successfully').build()
			return BaseController.json_response(response.__dict__, 200)
		except Exception as e:
			response_builder = response_builder.set_success(False)
			if e.args[0] == 'NotFound':
				response = response_builder.set_message('user does not exist').build()
				return BaseController.json_response(response.__dict__, 404)
			if e.args[0] == 'IncorrectOldPassword':
				response = response_builder.set_message('password provided does not match').build()
				return BaseController.json_response(response.__dict__, 422)			
			response = response_builder.set_message('unknown error occured, please try again later').build()
			return BaseController.json_response(response.__dict__, 500)
		

	def add(self, request):
		email = request.form['email'] if 'email' in request.form else None
		name = request.form['name'] if 'name' in request.form else None
		phone = request.form['phone'] if 'phone' in request.form else None
		password = request.form['password'] if 'password' in request.form else None
		if None in [email, name, phone, password]:
			response = ResponseBuilder() \
				.set_message('invalid payload') \
				.set_success(False) \
				.build()
			return BaseController.json_response(response.__dict__, 422)
		try:
			user = self.service.register({'email': email, 'name': name, 'phone': phone, 'password': password})
			response = ResponseBuilder() \
				.set_data(user) \
				.set_message('user registered successfully') \
				.build()
			return BaseController.json_response(response.__dict__, 201)
		except Exception as e:
			response_builder = ResponseBuilder().set_success(False)
			if e.args[0] == 'UserAlreadyExist':
				response = response_builder.set_message('user already exist').build()
				return BaseController.json_response(response.__dict__, 409)			
			response = response_builder.set_message('unknown error occured, please try again later').build()
			return BaseController.json_response(response.__dict__, 500)

	def login(self, request):
		email = request.form['email'] if 'email' in request.form else None
		password = request.form['password'] if 'password' in request.form else None
		if None in [email, password]:
			response = ResponseBuilder() \
				.set_success(False) \
				.set_message('invalid payload provided') \
				.build()
			return BaseController.json_response(response.__dict__, 422)
		try:
			access_token = self.service.login({'email': email, 'password': password})
			response = ResponseBuilder() \
				.set_message('user logged in successfully') \
				.set_data(access_token) \
				.build()
			return BaseController.json_response(response.__dict__, 201)
		except Exception as e:
			response_builder = ResponseBuilder().set_success(False)
			if e.args[0] == 'InvalidEmailOrPassword':
				response = response_builder.set_message('invalid email / password').build()
				return BaseController.json_response(response.__dict__, 409)
			response = response_builder.set_message('unknown error occured, please try again later').build()
			return BaseController.json_response(response.__dict__, 500)
